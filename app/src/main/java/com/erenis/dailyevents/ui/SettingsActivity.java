package com.erenis.dailyevents.ui;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.TextView;

import com.erenis.dailyevents.R;
import com.erenis.dailyevents.background.SampleAlarmReceiver;
import com.erenis.dailyevents.ui.fragments.ScheduleFragment;
import com.erenis.dailyevents.utils.Preferences;
import com.erenis.dailyevents.utils.Utils;
import com.facebook.FacebookSdk;
import com.facebook.Profile;
import com.facebook.login.LoginManager;

/**
 * Created by ereni on 25-Dec-16. TODO
 */

public class SettingsActivity extends AppCompatActivity
{
    private static final String TAG = SettingsActivity.class.getSimpleName();

    private CheckBox facebook;

    private CheckBox events;

    private CheckBox birthdays;

    private CheckBox google;

    private CheckBox notifications;

    private ScheduleFragment fragment;

    private TextView schedule;

    private TextView scheduleTime;

    private SampleAlarmReceiver sampleAlarmReceiver;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        FacebookSdk.sdkInitialize(getApplicationContext());
        setContentView(R.layout.activity_settings);

        fragment = new ScheduleFragment();
        fragment.setListener(mScheduleListener);
        sampleAlarmReceiver = new SampleAlarmReceiver();
        initViews();
    }

    @Override
    public void onBackPressed()
    {
        if (fragment != null && fragment.isVisible())
        {
            hideFragment();
        }
        else
        {
            super.onBackPressed();
        }
    }

    //--------------------------------------------------------------------------

    private void initViews()
    {
        facebook = (CheckBox) findViewById(R.id.widget_check_box_facebook);
        events = (CheckBox) findViewById(R.id.widget_check_box_facebook_events);
        birthdays = (CheckBox) findViewById(R.id.widget_check_box_facebook_birthday);
        google = (CheckBox) findViewById(R.id.widget_check_box_google);
        notifications = (CheckBox) findViewById(R.id.widget_check_box_notification);

        schedule = (TextView) findViewById(R.id.widget_text_view_schedule_settings);
        scheduleTime = (TextView) findViewById(R.id.widget_text_view_schedule_time_settings);

        loadData();

        facebook.setOnCheckedChangeListener(mOnCheckedChangeListener);
        events.setOnCheckedChangeListener(mOnCheckedChangeListener);
        birthdays.setOnCheckedChangeListener(mOnCheckedChangeListener);
        google.setOnCheckedChangeListener(mOnCheckedChangeListener);
        notifications.setOnCheckedChangeListener(mOnCheckedChangeListener);

        scheduleTime.setOnClickListener(mOnClickListener);
    }

    private void loadData()
    {
        if (Preferences.isChecked(getBaseContext(), 0))
        {
            facebook.setChecked(true);
        }
        else
        {
            events.setEnabled(false);
            birthdays.setEnabled(false);
        }
        if (Preferences.isChecked(getBaseContext(), 1))
        {
            events.setChecked(true);
        }
        if (Preferences.isChecked(getBaseContext(), 2))
        {
            birthdays.setChecked(true);
        }
        if (Preferences.isChecked(getBaseContext(), 3))
        {
            google.setChecked(true);
        }
        if (Preferences.isNotificationEnabled(getBaseContext()))
        {
            notifications.setChecked(true);

            int hours = Preferences.getHours(getBaseContext());
            int minutes = Preferences.getMinutes(getBaseContext());
            scheduleTime.setText(Utils.formatInt(hours) + ":" + Utils.formatInt(minutes));
        }
        else
        {
            schedule.setText(R.string.text_view_notification_disabled);
            sampleAlarmReceiver.cancelAlarm(this);
        }
    }

    private void handleFacebookChecks(boolean checked)
    {
        events.setEnabled(checked);
        birthdays.setEnabled(checked);

        if (!events.isChecked() && !birthdays.isChecked())
        {
            facebook.setChecked(false);
        }
    }

    private void logoutFromFacebook()
    {
        if (Profile.getCurrentProfile() != null)
        {
            LoginManager.getInstance().logOut();
            Preferences.setFacebookId(getBaseContext(), "");
            Preferences.setFacebookToken(getBaseContext(), "");
        }
    }

    private void handleNotificationCheckBox(boolean isChecked)
    {
        if (isChecked)
        {
            int hour = Preferences.getHours(getBaseContext());
            int minute = Preferences.getMinutes(getBaseContext());
            schedule.setText(getString(R.string.text_view_schedule));
            scheduleTime.setText(Utils.formatLong(hour) + ":" + Utils.formatLong(minute));

            showFragment();
        }
        else
        {
            schedule.setText(getString(R.string.text_view_notification_disabled));
            sampleAlarmReceiver.cancelAlarm(SettingsActivity.this);
            scheduleTime.setText("");
        }
    }

    private void showFragment()
    {
        findViewById(R.id.widget_layout_settings).setAlpha(0.5f);
        getSupportFragmentManager().beginTransaction().add(android.R.id.content, fragment, fragment.getTag()).commit();
    }

    private void hideFragment()
    {
        findViewById(R.id.widget_layout_settings).setAlpha(1f);
        getSupportFragmentManager().beginTransaction().remove(fragment).commit();
    }

    //----------------------------- Listeners ----------------------------------

    private CompoundButton.OnCheckedChangeListener mOnCheckedChangeListener = new CompoundButton.OnCheckedChangeListener()
    {
        @Override
        public void onCheckedChanged(final CompoundButton compoundButton, boolean b)
        {
            int id = compoundButton.getId();

            switch (id)
            {
                case R.id.widget_check_box_facebook:
                    if (!b)
                    {
                        logoutFromFacebook();
                    }
                    handleFacebookChecks(b);
                    Preferences.setChecked(getBaseContext(), 0, b);
                    break;
                case R.id.widget_check_box_facebook_events:
                    handleFacebookChecks(b);
                    Preferences.setChecked(getBaseContext(), 1, b);
                    break;
                case R.id.widget_check_box_facebook_birthday:
                    handleFacebookChecks(b);
                    Preferences.setChecked(getBaseContext(), 2, b);
                    break;
                case R.id.widget_check_box_google:
                    Preferences.setChecked(getBaseContext(), 3, b);
                    break;
                case R.id.widget_check_box_notification:
                    Preferences.setNotifications(getBaseContext(), b);
                    handleNotificationCheckBox(b);
                    break;
            }
        }
    };

    private View.OnClickListener mOnClickListener = new View.OnClickListener()
    {
        @Override
        public void onClick(View v)
        {
            int id = v.getId();
            switch (id)
            {
                case R.id.widget_text_view_schedule_time_settings:
                    showFragment();
                    break;
            }
        }
    };

    private ScheduleFragment.ScheduleListener mScheduleListener = new ScheduleFragment.ScheduleListener()
    {
        @Override
        public void onReschedule(int hour, int minute)
        {
            Preferences.setHours(getBaseContext(), hour);
            Preferences.setMinutes(getBaseContext(), minute);
            scheduleTime.setText(Utils.formatInt(hour) + ":" + Utils.formatInt(minute));
            sampleAlarmReceiver.cancelAlarm(SettingsActivity.this);
            sampleAlarmReceiver.setAlarm(SettingsActivity.this, hour, minute);
            Preferences.setFirstTime(getBaseContext(), true);
            hideFragment();
        }
    };
}