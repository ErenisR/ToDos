package com.erenis.dailyevents.ui;

import android.Manifest;
import android.accounts.AccountManager;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import com.erenis.dailyevents.R;
import com.erenis.dailyevents.adapters.ToDosAdapter;
import com.erenis.dailyevents.background.MakeRequestTask;
import com.erenis.dailyevents.models.Event;
import com.erenis.dailyevents.ui.fragments.EventDetailsFragment;
import com.erenis.dailyevents.utils.Preferences;
import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.HttpMethod;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.google.api.client.googleapis.extensions.android.gms.auth.GoogleAccountCredential;
import com.google.api.client.googleapis.extensions.android.gms.auth.GooglePlayServicesAvailabilityIOException;
import com.google.api.client.googleapis.extensions.android.gms.auth.UserRecoverableAuthIOException;
import com.google.api.client.util.ExponentialBackOff;
import com.google.api.services.calendar.CalendarScopes;

import org.json.JSONArray;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;

import pub.devrel.easypermissions.AfterPermissionGranted;
import pub.devrel.easypermissions.EasyPermissions;


public class MainActivity extends AppCompatActivity
{
    private static final String TAG = MainActivity.class.getSimpleName();

    private static final int REQUEST_ACCOUNT_PICKER = 1000;

    private static final int REQUEST_AUTHORIZATION = 1001;

    private static final int REQUEST_GOOGLE_PLAY_SERVICES = 1002;

    private static final int REQUEST_PERMISSION_GET_ACCOUNTS = 1003;

    private CallbackManager mCallbackManager;

    private GoogleAccountCredential mCredential;

    private ListView mToDos;

    private ToDosAdapter mToDosAdapter;

    private EventDetailsFragment fragment;

    private int allSources = 0;

    private int counter = 0;

    private List<Event> mList;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        mCallbackManager = CallbackManager.Factory.create();
        mCredential = GoogleAccountCredential.usingOAuth2(getApplicationContext(), Collections
                .singletonList(CalendarScopes.CALENDAR_READONLY)).setBackOff(new ExponentialBackOff());
        setContentView(R.layout.activity_main);
        if (!Preferences.isAppUsed(getBaseContext()))
        {
            startActivity(new Intent(this, SettingsActivity.class));
        }

        fragment = new EventDetailsFragment();
        mList = new ArrayList<>();

        initViews();
        initRequests();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu)
    {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings)
        {
            Intent intent = new Intent(MainActivity.this, SettingsActivity.class);
            startActivity(intent);
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed()
    {
        if (fragment != null && fragment.isVisible())
        {
            hideFragment();
        }
        else
        {
            super.onBackPressed();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data)
    {
        super.onActivityResult(requestCode, resultCode, data);
        if (mCallbackManager != null)
        {
            mCallbackManager.onActivityResult(requestCode, resultCode, data);
        }
        switch (requestCode)
        {
            case REQUEST_GOOGLE_PLAY_SERVICES:
                if (resultCode != RESULT_OK)
                {
                    Toast.makeText(MainActivity.this,
                                   "This app requires Google Play Services. Please install Google Play Services on your device and relaunch this app.",
                                   Toast.LENGTH_SHORT).show();
                }
                else
                {
                    getResultsFromGoogleApi();
                }
                break;
            case REQUEST_ACCOUNT_PICKER:
                if (resultCode == RESULT_OK && data != null && data.getExtras() != null)
                {
                    String accountName = data.getStringExtra(AccountManager.KEY_ACCOUNT_NAME);
                    if (accountName != null)
                    {
                        Preferences.setGoogleAccountName(getBaseContext(), accountName);
                        getResultsFromGoogleApi();
                    }
                }
                break;
            case REQUEST_AUTHORIZATION:
                if (resultCode == RESULT_OK)
                {
                    getResultsFromGoogleApi();
                }
                break;
        }
    }

    //------------------------------------------------------------------------

    private void initViews()
    {
        Toolbar toolbar = (Toolbar) findViewById(R.id.widget_toolbar_main_activity);
        setSupportActionBar(toolbar);

        mToDos = (ListView) findViewById(R.id.widget_list_to_do);
        mToDos.setOnItemClickListener(mOnItemClickListener);
    }

    private void initRequests()
    {
        if (Preferences.isChecked(getBaseContext(), 0))
        {
            initFacebook();
        }
        if (Preferences.isChecked(getBaseContext(), 3))
        {
            getResultsFromGoogleApi();
            allSources++;
        }
    }

    private void initFacebook()
    {
        List<String> facebookPermission = new ArrayList<>();
        if (Preferences.isChecked(getBaseContext(), 2))
        {
            facebookPermission.add("user_friends");
            allSources++;
        }
        if (Preferences.isChecked(getBaseContext(), 1))
        {
            facebookPermission.add("user_events");
            allSources++;
        }
//        facebookPermission.add("user_birthday");
        LoginManager.getInstance().registerCallback(mCallbackManager, mFacebookCallback);
        LoginManager.getInstance().logInWithReadPermissions(MainActivity.this, facebookPermission);
    }

    private void showFragment()
    {
        getSupportFragmentManager().beginTransaction().add(android.R.id.content, fragment).commit();
    }

    private void hideFragment()
    {
        getSupportFragmentManager().beginTransaction().remove(fragment).commit();
    }

    /**
     * Attempt to call the API, after verifying that all the preconditions are
     * satisfied. The preconditions are: Google Play Services installed, an
     * account was selected and the device currently has online access. If any
     * of the preconditions are not satisfied, the app will prompt the user as
     * appropriate.
     */
    private void getResultsFromGoogleApi()
    {
        if (!isGooglePlayServicesAvailable())
        {
            acquireGooglePlayServices();
        }
        else if (mCredential.getSelectedAccountName() == null)
        {
            chooseAccount();
        }
        else if (!isDeviceOnline())
        {
            Toast.makeText(MainActivity.this, "No internet connection!", Toast.LENGTH_SHORT).show();
        }
        else if (!EasyPermissions.hasPermissions(this, Manifest.permission.GET_ACCOUNTS))
        {
            Toast.makeText(MainActivity.this, "This app needs to access your Google account (via Contacts).", Toast.LENGTH_SHORT).show();
        }
        else
        {
            makeCalendarRequest();
        }
    }

    /**
     * Checks whether the device currently has a network connection.
     *
     * @return true if the device has a network connection, false otherwise.
     */
    private boolean isDeviceOnline()
    {
        ConnectivityManager connMgr = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connMgr.getActiveNetworkInfo();
        return (networkInfo != null && networkInfo.isConnected());
    }

    /**
     * Check that Google Play services APK is installed and up to date.
     *
     * @return true if Google Play Services is available and up to
     * date on this device; false otherwise.
     */
    private boolean isGooglePlayServicesAvailable()
    {
        GoogleApiAvailability apiAvailability = GoogleApiAvailability.getInstance();
        final int connectionStatusCode = apiAvailability.isGooglePlayServicesAvailable(this);
        return connectionStatusCode == ConnectionResult.SUCCESS;
    }

    /**
     * Attempt to resolve a missing, out-of-date, invalid or disabled Google
     * Play Services installation via a user dialog, if possible.
     */
    private void acquireGooglePlayServices()
    {
        GoogleApiAvailability apiAvailability = GoogleApiAvailability.getInstance();
        final int connectionStatusCode = apiAvailability.isGooglePlayServicesAvailable(this);
        if (apiAvailability.isUserResolvableError(connectionStatusCode))
        {
            showGooglePlayServicesAvailabilityErrorDialog(connectionStatusCode);
        }
    }

    /**
     * Attempts to set the account used with the API credentials. If an account
     * name was previously saved it will use that one; otherwise an account
     * picker dialog will be shown to the user. Note that the setting the
     * account to use with the credentials object requires the app to have the
     * GET_ACCOUNTS permission, which is requested here if it is not already
     * present. The AfterPermissionGranted annotation indicates that this
     * function will be rerun automatically whenever the GET_ACCOUNTS permission
     * is granted.
     */
    @AfterPermissionGranted(REQUEST_PERMISSION_GET_ACCOUNTS)
    private void chooseAccount()
    {
        if (EasyPermissions.hasPermissions(this, Manifest.permission.GET_ACCOUNTS))
        {
//            String accountName = getPreferences(Context.MODE_PRIVATE).getString(PREF_ACCOUNT_NAME, null);
            String accountName = Preferences.getGoogleAccountName(getBaseContext());
            if (!accountName.equals(""))
            {
                mCredential.setSelectedAccountName(accountName);
                getResultsFromGoogleApi();
            }
            else
            {
                // Start a dialog from which the user can choose an account
                startActivityForResult(mCredential.newChooseAccountIntent(), REQUEST_ACCOUNT_PICKER);
            }
        }
        else
        {
            // Request the GET_ACCOUNTS permission via a user dialog
            EasyPermissions.requestPermissions(this, "This app needs to access your Google account (via Contacts).", REQUEST_PERMISSION_GET_ACCOUNTS,
                                               Manifest.permission.GET_ACCOUNTS);
        }
    }

    /**
     * Display an error dialog showing that Google Play Services is missing or out of date.
     *
     * @param connectionStatusCode code describing the presence (or lack of) Google Play Services on this device.
     */
    void showGooglePlayServicesAvailabilityErrorDialog(final int connectionStatusCode)
    {
        GoogleApiAvailability apiAvailability = GoogleApiAvailability.getInstance();
        Dialog dialog = apiAvailability.getErrorDialog(MainActivity.this, connectionStatusCode, REQUEST_GOOGLE_PLAY_SERVICES);
        dialog.show();
    }

    private void makeEventGraphRequest(AccessToken token)
    {
        Log.d(TAG, "Event GraphRequest");
        Bundle bundle = new Bundle();
        bundle.putString("fields", "name, id, description, start_time, end_time, rsvp_status, place");
        GraphRequest eventsRequest = new GraphRequest(token, "/" + token.getUserId() + "/events", bundle, HttpMethod.GET, new GraphRequest.Callback()
        {
            @Override
            public void onCompleted(GraphResponse response)
            {
                counter++;
                Log.d(TAG, "Event GraphRequest Completed");
                JSONArray array = response.getJSONObject().optJSONArray("data");
                for (int i = 0; i < array.length(); i++)
                {
                    Event event = new Event(array.optJSONObject(i), Event.EventType.FACEBOOK_EVENT);
                    Log.d(TAG, "Event time: " + event.getStartTime());
                    if (checkIfItIsToday(event.getStartDate()))
                    {
                        mList.add(event);
                    }
                }
                checkAndSetAdapter();
            }
        });
        eventsRequest.executeAsync();
    }

    private void makeFriendsGraphRequest(final AccessToken token)
    {
        Log.d(TAG, "Friends GraphRequest");
        Bundle bundle = new Bundle();
        bundle.putString("fields", "name, id, birthday");

        GraphRequest request = new GraphRequest(token, "/" + token.getUserId() + "/friends", bundle, HttpMethod.GET, new GraphRequest.Callback()
        {
            @Override
            public void onCompleted(GraphResponse response)
            {
                counter++;
                Log.d(TAG, "Friends GraphRequest Completed");
                Log.d(TAG, "OnFriendsRequest: " + response.getJSONObject());
                checkAndSetAdapter();
            }
        });
        request.executeAsync();
    }

    private void makeCalendarRequest()
    {
        new MakeRequestTask(mCredential, mGoogleCallback).execute();
    }

    private void checkAndSetAdapter()
    {
        if (counter == allSources)
        {
            if (mList.size() > 0)
            {
                Collections.sort(mList, new EventComparator());
                mToDosAdapter = new ToDosAdapter(this, mList);
                mToDos.setAdapter(mToDosAdapter);
            }
            else
            {
                findViewById(R.id.widget_text_view_no_events).setVisibility(View.VISIBLE);
            }
        }
    }

    private boolean checkIfItIsToday(Date eventDate)
    {
        Calendar eventCalendar = Calendar.getInstance();
        eventCalendar.setTime(eventDate);
        Calendar today = Calendar.getInstance();

        return (today.get(Calendar.DAY_OF_YEAR) == eventCalendar.get(Calendar.DAY_OF_YEAR))
                && (today.get(Calendar.YEAR) == eventCalendar.get(Calendar.YEAR));
    }

    //---------------------- Listeners --------------------------------------------------

    private AdapterView.OnItemClickListener mOnItemClickListener = new AdapterView.OnItemClickListener()
    {
        @Override
        public void onItemClick(AdapterView<?> adapterView, View view, int i, long l)
        {
            Event event = (Event) mToDosAdapter.getItem(i);
            Bundle bundle = new Bundle();
//            bundle.putString("id", event.getId());
            bundle.putSerializable("event", event);
            fragment.setArguments(bundle);

            showFragment();
        }
    };

    private FacebookCallback<LoginResult> mFacebookCallback = new FacebookCallback<LoginResult>()
    {
        @Override
        public void onSuccess(LoginResult loginResult)
        {
            Log.d(TAG, "OnSuccess Facebook");
            final AccessToken token = loginResult.getAccessToken();
            Log.d(TAG, "Token: " + token.getToken());
            Log.d(TAG, "ID: " + token.getUserId());
            Preferences.setFacebookToken(getBaseContext(), token.getToken());
            Preferences.setFacebookId(getBaseContext(), token.getUserId());

            for (String s : loginResult.getRecentlyGrantedPermissions())
            {
                Log.d(TAG, "Permission: " + s);
            }

            if (Preferences.isChecked(getBaseContext(), 1))
            {
                makeEventGraphRequest(token);
            }
            if (Preferences.isChecked(getBaseContext(), 2))
            {
                makeFriendsGraphRequest(token);
            }
        }

        @Override
        public void onCancel()
        {
            Log.d(TAG, "onCancel Facebook");
        }

        @Override
        public void onError(FacebookException error)
        {
            Log.d(TAG, "onError Facebook");
        }
    };

    private MakeRequestTask.GoogleCalendarListener mGoogleCallback = new MakeRequestTask.GoogleCalendarListener()
    {
        @Override
        public void onSuccess(List<com.google.api.services.calendar.model.Event> response)
        {
            counter++;
            for (com.google.api.services.calendar.model.Event e : response)
            {
                if (checkIfItIsToday(new Date(e.getStart().getDateTime().getValue())))
                {
                    Event event = new Event(e);
                    mList.add(event);
                }
            }
            checkAndSetAdapter();
        }

        @Override
        public void onFailed()
        {
            counter++;
            Toast.makeText(MainActivity.this, "No data returned from Calendar!", Toast.LENGTH_SHORT).show();
        }

        @Override
        public void onCanceled(Exception exception)
        {
            if (exception != null)
            {
                if (exception instanceof GooglePlayServicesAvailabilityIOException)
                {
                    counter++;
                    showGooglePlayServicesAvailabilityErrorDialog(
                            ((GooglePlayServicesAvailabilityIOException) exception).getConnectionStatusCode());
                }
                else if (exception instanceof UserRecoverableAuthIOException)
                {
                    startActivityForResult(((UserRecoverableAuthIOException) exception).getIntent(), TestActivity.REQUEST_AUTHORIZATION);
                }
                else
                {
                    counter++;
                    Toast.makeText(MainActivity.this, "The following error occurred:\n" + exception.getMessage(), Toast.LENGTH_SHORT).show();
                }
            }
            else
            {
                counter++;
                Toast.makeText(MainActivity.this, "Request canceled!", Toast.LENGTH_SHORT).show();
            }
        }
    };

    public class EventComparator implements Comparator<Event>
    {
        @Override
        public int compare(Event event, Event t1)
        {
            return event.getStartDate().compareTo(t1.getStartDate());
        }
    }
}