package com.erenis.dailyevents.ui.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.NumberPicker;

import com.erenis.dailyevents.R;
import com.erenis.dailyevents.utils.Preferences;

/**
 * Created by ereni on 19-Jan-17.
 */

public class ScheduleFragment extends Fragment
{
    public interface ScheduleListener
    {
        void onReschedule(int hour, int minute);
    }

    private static final String TAG = ScheduleFragment.class.getSimpleName();

    private NumberPicker hours;

    private NumberPicker minutes;

    private ScheduleListener mListener;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState)
    {
        View view = inflater.inflate(R.layout.fragment_schedule, container, false);
        initViews(view);
        return view;
    }

    //----------------------------------------------------------------------------------------

    public void setListener(ScheduleListener listener)
    {
        mListener = listener;
    }

    private void initViews(View view)
    {
        hours = (NumberPicker) view.findViewById(R.id.widget_number_picket_hours_schedule);
        minutes = (NumberPicker) view.findViewById(R.id.widget_number_picket_minutes_schedule);

        Button done = (Button) view.findViewById(R.id.widget_button_done_schedule);
        done.setOnClickListener(mOnClickListener);

        hours.setMinValue(0);
        hours.setMaxValue(23);

        minutes.setMinValue(0);
        minutes.setMaxValue(59);

        int hour = Preferences.getHours(getContext());
        int minute = Preferences.getMinutes(getContext());

        hours.setValue(hour);
        minutes.setValue(minute);
    }

    private void handleNewValue()
    {
        int hour = hours.getValue();
        int minute = minutes.getValue();

        mListener.onReschedule(hour, minute);
    }

    //----------------------------------------------------------------------------------------

    private View.OnClickListener mOnClickListener = new View.OnClickListener()
    {
        @Override
        public void onClick(View v)
        {
            int id = v.getId();
            switch (id)
            {
                case R.id.widget_button_done_schedule:
                    handleNewValue();
                    break;
            }
        }
    };
}
