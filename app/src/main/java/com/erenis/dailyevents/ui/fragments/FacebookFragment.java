package com.erenis.dailyevents.ui.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.preference.PreferenceFragment;
import android.view.MenuItem;

import com.erenis.dailyevents.R;
import com.erenis.dailyevents.ui.SettingsActivity;

/**
 * Created by ereni on 14-Jan-17. TODO:
 */

public class FacebookFragment extends PreferenceFragment
{
    /*public interface FacebookFragmentListener
    {
        void onFacebookFragmentCreated(String preference);
    }

    private FacebookFragmentListener mListener;

    public FacebookFragment()
    {
    }

    public void registerListener(FacebookFragmentListener listener)
    {
        mListener = listener;
    }*/

    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        addPreferencesFromResource(R.xml.facebook_preferences);
        findPreference("facebook_preferences");
       /* mListener.onFacebookFragmentCreated("facebook_preferences");*/
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        int id = item.getItemId();
        if (id == android.R.id.home)
        {
            startActivity(new Intent(getActivity(), SettingsActivity.class));
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
