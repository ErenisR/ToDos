package com.erenis.dailyevents.ui.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.preference.PreferenceFragment;
import android.view.MenuItem;

import com.erenis.dailyevents.R;
import com.erenis.dailyevents.ui.SettingsActivity;

/**
 * Created by ereni on 14-Jan-17.
 */

public class GoogleFragment extends PreferenceFragment
{
    /*public interface GoogleFragmentListener
    {
        void onGoogleFragmentCreated(String preference);
    }

    private GoogleFragmentListener mListener;

    public GoogleFragment()
    {
    }

    public void registerLisener(GoogleFragmentListener listener)
    {
        mListener = listener;
    }
*/
    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        addPreferencesFromResource(R.xml.google_preferences);

        findPreference("google_preferences");
        /*mListener.onGoogleFragmentCreated("google_preferences");*/
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        int id = item.getItemId();
        if (id == android.R.id.home)
        {
            startActivity(new Intent(getActivity(), SettingsActivity.class));
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
