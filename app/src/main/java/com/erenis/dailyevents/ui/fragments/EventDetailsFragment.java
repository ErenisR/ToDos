package com.erenis.dailyevents.ui.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.method.ScrollingMovementMethod;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.erenis.dailyevents.R;
import com.erenis.dailyevents.models.Event;
import com.erenis.dailyevents.utils.Utils;

/**
 * Created by ereni on 15-Jan-17. TODO:
 */

public class EventDetailsFragment extends Fragment
{
    private TextView title;

    private TextView description;

    private TextView status;

    private TextView location;

    private TextView startTime;

    private TextView endTime;

    private TextView source;

    private Event event;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        Bundle args = getArguments();
//        event = Utils.events.get(args.getString("id"));
        event = (Event) args.getSerializable("event");
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState)
    {
        View view = inflater.inflate(R.layout.fragment_details, container, false);
        initViews(view);
        loadData();
        return view;
    }

    //----------------------------------------------------------------------------------------------------------------------------------------

    private void initViews(View view)
    {
        title = (TextView) view.findViewById(R.id.widget_text_view_title_details);
        description = (TextView) view.findViewById(R.id.widget_text_view_description_details);
        status = (TextView) view.findViewById(R.id.widget_text_view_status_details);
        location = (TextView) view.findViewById(R.id.widget_text_view_location_details);
        startTime = (TextView) view.findViewById(R.id.widget_text_view_start_time_details);
        endTime = (TextView) view.findViewById(R.id.widget_text_view_end_time_details);
        source = (TextView) view.findViewById(R.id.widget_text_view_source_details);

        description.setMovementMethod(new ScrollingMovementMethod());
    }

    private void loadData()
    {
        title.setText(event.getName());
        description.setText(event.getDescription());
        startTime.setText(event.getStartTime());
        endTime.setText(event.getEndTime());
        status.setText(event.getRsvpStatus());
        location.setText(event.getPlace());
        source.setText(event.getType().toString());
    }
}
