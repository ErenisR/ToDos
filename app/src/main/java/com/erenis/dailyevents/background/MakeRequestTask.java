package com.erenis.dailyevents.background;

import android.os.AsyncTask;
import android.text.TextUtils;

import com.erenis.dailyevents.utils.Utils;
import com.google.api.client.extensions.android.http.AndroidHttp;
import com.google.api.client.googleapis.extensions.android.gms.auth.GoogleAccountCredential;
import com.google.api.client.http.HttpTransport;
import com.google.api.client.json.JsonFactory;
import com.google.api.client.json.jackson2.JacksonFactory;
import com.google.api.client.util.DateTime;
import com.google.api.services.calendar.model.Event;
import com.google.api.services.calendar.model.Events;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

/**
 * Created by ereni on 16-Jan-17. TODO:
 */

public class MakeRequestTask extends AsyncTask<Void, Void, List<Event>>
{
    public interface GoogleCalendarListener
    {
        void onSuccess(List<Event> response);

        void onFailed();

        void onCanceled(Exception exception);
    }

    private com.google.api.services.calendar.Calendar mService = null;

    private Exception mLastError = null;

    private GoogleCalendarListener mListener;

    public MakeRequestTask(GoogleAccountCredential credential, GoogleCalendarListener listener)
    {
        HttpTransport transport = AndroidHttp.newCompatibleTransport();
        JsonFactory jsonFactory = JacksonFactory.getDefaultInstance();
        mService = new com.google.api.services.calendar.Calendar.Builder(transport, jsonFactory, credential).setApplicationName("ToDos").build();
        mListener = listener;
    }

    /**
     * Background task to call Google Calendar API.
     *
     * @param params no parameters needed for this task.
     */
    @Override
    protected List<Event> doInBackground(Void... params)
    {
        try
        {
            return getDataFromApi();
        }
        catch (Exception e)
        {
            mLastError = e;
            cancel(true);
            return null;
        }
    }

    /**
     * Fetch a list of the next 10 events from the primary calendar.
     *
     * @return List of Strings describing returned events.
     * @throws IOException
     */
    private List<Event> getDataFromApi() throws IOException
    {
        // List the next 10 events from the primary calendar.
        DateTime now = new DateTime(System.currentTimeMillis());
        Events events = mService.events().list("primary")
                .setMaxResults(10)
                .setTimeMin(now)
                .setOrderBy("startTime")
                .setSingleEvents(true)
                .execute();
        return events.getItems();
    }


    @Override
    protected void onPostExecute(List<Event> output)
    {
        if (output == null || output.size() == 0)
        {
            mListener.onFailed();
        }
        else
        {
            mListener.onSuccess(output);
        }
    }

    @Override
    protected void onCancelled()
    {
        mListener.onCanceled(mLastError);
    }
}