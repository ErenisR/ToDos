package com.erenis.dailyevents.background;

import android.app.Service;
import android.content.Intent;
import android.os.Handler;
import android.os.IBinder;
import android.support.annotation.Nullable;

/**
 * Created by ereni on 25-Dec-16.
 */

public class BackgroundService extends Service
{

    @Override
    public void onCreate()
    {
        super.onCreate();
        Handler handler = new Handler();
        handler.postDelayed(runnable, 1000);
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent)
    {
        return null;
    }

    //-----------------------------------------------------------------------------------

    private Runnable runnable = new Runnable()
    {
        @Override
        public void run()
        {

        }
    };
}
