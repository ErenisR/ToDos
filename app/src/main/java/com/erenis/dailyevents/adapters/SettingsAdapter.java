package com.erenis.dailyevents.adapters;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.erenis.dailyevents.R;
import com.erenis.dailyevents.utils.Preferences;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Created by ereni on 14-Jan-17. TODO:
 */

public class SettingsAdapter extends BaseAdapter
{
    public interface SettingsAdapterListener
    {
        void onCheckedChange(int position, boolean value);
    }

    private static String TAG = SettingsAdapter.class.getSimpleName();

    private Context mContext;

    private List<String> mList;

    private SettingsAdapterListener mListener;

    private boolean isCanceled = false;

    public SettingsAdapter(Context context, SettingsAdapterListener listener)
    {
        mContext = context;
        mList = new ArrayList<>();
        Collections.addAll(mList, context.getResources().getStringArray(R.array.resources));

        mListener = listener;
    }

    @Override
    public int getCount()
    {
        return mList.size();
    }

    @Override
    public Object getItem(int i)
    {
        return mList.get(i);
    }

    @Override
    public long getItemId(int i)
    {
        return 0;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup)
    {
        Holder holder;
        String title = (String) getItem(i);
        if (view == null)
        {
            holder = new Holder();
            if (!title.equals("Facebook") || !title.equals("Google"))
            {
                view = LinearLayout.inflate(mContext, R.layout.settings_adapter_item_margins, null);
            }
            else
            {
                view = LinearLayout.inflate(mContext, R.layout.settings_adapter_item, null);
            }


            holder.title = (TextView) view.findViewById(R.id.widget_text_view_settings_item_title);
            holder.checkBox = (CheckBox) view.findViewById(R.id.widget_check_box_settings_item);

            holder.checkBox.setOnCheckedChangeListener(null);
            view.setOnClickListener(mOnClickListener(holder, i));
            view.setTag(holder);
        }
        else
        {
            holder = (Holder) view.getTag();
        }

        holder.title.setText(title);
        holder.checkBox.setChecked(isChecked(i));

        return view;
    }

    private boolean isChecked(int position)
    {
        return false;
//        return Preferences.isChecked(mContext, position) && !isCanceled;
    }

    private View.OnClickListener mOnClickListener(final Holder holder, final int position)
    {
        return new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                boolean checked = !isChecked(position);
                holder.checkBox.setChecked(checked);
                mListener.onCheckedChange(position, checked);
            }
        };
    }

    public void cancel(boolean cancel)
    {
        isCanceled = cancel;
        notifyDataSetChanged();
    }

    private class Holder
    {
        TextView title;

        CheckBox checkBox;
    }
}
