package com.erenis.dailyevents.adapters;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.erenis.dailyevents.R;
import com.erenis.dailyevents.models.Event;
import com.erenis.dailyevents.utils.Utils;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by ereni on 25-Dec-16. TODO:
 */

public class ToDosAdapter extends BaseAdapter
{

    private static String TAG = ToDosAdapter.class.getSimpleName();

    private Context mContext;

    private List<Event> mList;

    public ToDosAdapter(Context context, List<Event> list)
    {
        mContext = context;
        mList = list;
//        mList.addAll(Utils.events.values());
    }

    @Override
    public int getCount()
    {
        return mList.size();
    }

    @Override
    public Object getItem(int i)
    {
        return mList.get(i);
    }

    @Override
    public long getItemId(int i)
    {
        return 0;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup)
    {
        ToDosHolder holder;
        Event event = mList.get(i);
        if (view == null)
        {
            holder = new ToDosHolder();
            view = LayoutInflater.from(mContext).inflate(R.layout.todo_adapter_facebook_item, null);
            holder.title = (TextView) view.findViewById(R.id.widget_text_view_todo_title);
            holder.status = (TextView) view.findViewById(R.id.widget_text_view_todo_status);
            holder.start = (TextView) view.findViewById(R.id.widget_text_view_todo_start_time);
            holder.end = (TextView) view.findViewById(R.id.widget_text_view_todo_end_time);
            holder.location = (TextView) view.findViewById(R.id.widget_text_view_todo_location);
            view.setTag(holder);
        }
        else
        {
            holder = (ToDosHolder) view.getTag();
        }

        holder.title.setText(event.getName());
        holder.status.setText("Status:".concat(event.getRsvpStatus()));

        //TODO: handle null Date

        holder.start.setText("Starting:\n".concat(event.getStartTime()));
        holder.end.setText("Ending:\n".concat(event.getEndTime()));
        holder.location.setText("Location:\n".concat(event.getPlace()));

        return view;
    }

    private class ToDosHolder
    {
        TextView title;
        TextView status;
        TextView start;
        TextView end;
        TextView location;
    }
}
