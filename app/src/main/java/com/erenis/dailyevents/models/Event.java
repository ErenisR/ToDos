package com.erenis.dailyevents.models;

import com.erenis.dailyevents.utils.Utils;

import org.json.JSONObject;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

/**
 * Created by ereni on 09-Jan-17. TODO:
 */

public class Event implements Serializable
{
    public enum EventType
    {
        FACEBOOK_EVENT, FACEBOOK_BIRTHDAY, GOOGLE_CALENDAR;

        @Override
        public String toString()
        {
            switch (this)
            {
                case FACEBOOK_BIRTHDAY:
                    return "Facebook Birthday";
                case FACEBOOK_EVENT:
                    return "Facebook event";
                case GOOGLE_CALENDAR:
                    return "Google Calendar";
                default:
                    return "";
            }
        }
    }

    private static final String TAG = Event.class.getSimpleName();

    private String id;

    private String description;

    private String name;

    private Date endTime;

    private Date startTime;

    private String rsvpStatus;

    private String place;

    private EventType type;

    public Event(JSONObject object, EventType type)
    {
        this.description = object.optString("description");
        this.id = object.optString("id");
        this.name = object.optString("name");
        this.endTime = Utils.convertToDate(object.optString("end_time"), "yyyy-MM-dd'T'HH:mm:ssZ");
        this.startTime = Utils.convertToDate(object.optString("start_time"), "yyyy-MM-dd'T'HH:mm:ssZ");
        this.rsvpStatus = object.optString("rsvp_status");
        if (object.has("place"))
        {
            this.place = object.optJSONObject("place").optString("name");
        }

        this.type = type;
    }

    public Event(com.google.api.services.calendar.model.Event event)
    {
        this.description = event.getDescription();
        this.id = event.getId();
        this.name = event.getSummary();
        this.endTime = new Date(event.getEnd().getDateTime().getValue());
        this.startTime = new Date(event.getStart().getDateTime().getValue());
        this.rsvpStatus = event.getStatus();
        this.place = event.getLocation();

        this.type = EventType.GOOGLE_CALENDAR;
    }

    public String getId()
    {
        return id;
    }

    public void setId(String id)
    {
        this.id = id;
    }

    public String getDescription()
    {
        return description;
    }

    public void setDescription(String description)
    {
        this.description = description;
    }

    public String getName()
    {
        return name;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public String getEndTime()
    {
        SimpleDateFormat sdf = new SimpleDateFormat("EEE, MMM dd HH:mm", Locale.ENGLISH);
        return endTime == null ? "No specified time" : sdf.format(endTime);
//        return endTime;
    }

    public Date getEndDate()
    {
        return endTime;
    }

    public long getEndTimeMillis()
    {
        return endTime.getTime();
    }

    public void setEndTime(Date endTime)
    {
        this.endTime = endTime;
    }

    public String getStartTime()
    {
        SimpleDateFormat sdf = new SimpleDateFormat("EEE, MMM dd HH:mm", Locale.ENGLISH);
        return startTime == null ? "No specified time" : sdf.format(startTime);
//        return startTime;
    }

    public Date getStartDate()
    {
        return startTime;
    }

    public long getStartTimeMillis()
    {
        return startTime.getTime();
    }

    public void setStartTime(Date startTime)
    {
        this.startTime = startTime;
    }

    public String getRsvpStatus()
    {
        return rsvpStatus;
    }

    public void setRsvpStatus(String rsvpStatus)
    {
        this.rsvpStatus = rsvpStatus;
    }

    public EventType getType()
    {
        return type;
    }

    public void setType(EventType type)
    {
        this.type = type;
    }

    public String getPlace()
    {
        return place == null ? "Unknown" : place;
    }

    public void setPlace(String place)
    {
        this.place = place;
    }
}
