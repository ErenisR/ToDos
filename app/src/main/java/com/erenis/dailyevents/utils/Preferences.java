package com.erenis.dailyevents.utils;

import android.content.Context;
import android.preference.PreferenceManager;

/**
 * Created by ereni on 25-Dec-16. TODO:
 */

public class Preferences
{

    public static void setFacebookToken(Context baseContext, String token)
    {
        PreferenceManager.getDefaultSharedPreferences(baseContext).edit().putString("facebookToken", token).apply();
    }

    public static String getFacebookToken(Context baseContext)
    {
        return PreferenceManager.getDefaultSharedPreferences(baseContext).getString("facebookToken", "");
    }

    public static void setFacebookId(Context baseContext, String id)
    {
        PreferenceManager.getDefaultSharedPreferences(baseContext).edit().putString("facebookId", id).apply();
    }

    public static String getFacebookId(Context baseContext)
    {
        return PreferenceManager.getDefaultSharedPreferences(baseContext).getString("facebookId", "");
    }

    public static void setChecked(Context baseContext, int id, boolean isChecked)
    {
        PreferenceManager.getDefaultSharedPreferences(baseContext).edit().putBoolean(id + "", isChecked).apply();
    }

    public static boolean isChecked(Context baseContext, int id)
    {
        return PreferenceManager.getDefaultSharedPreferences(baseContext).getBoolean(id + "", true);
    }

    public static void setGoogleAccountName(Context baseContext, String accountName)
    {
        PreferenceManager.getDefaultSharedPreferences(baseContext).edit().putString("googleAccountName", accountName).apply();
    }

    public static String getGoogleAccountName(Context baseContext)
    {
        return PreferenceManager.getDefaultSharedPreferences(baseContext).getString("googleAccountName", "");
    }

    public static void setFirstTime(Context baseContext, boolean firstTime)
    {
        PreferenceManager.getDefaultSharedPreferences(baseContext).edit().putBoolean("firstTime", firstTime).apply();
    }

    public static boolean isAppUsed(Context baseContext)
    {
        return PreferenceManager.getDefaultSharedPreferences(baseContext).getBoolean("firstTime", false);
    }

    public static void setNotifications(Context baseContext, boolean enabled)
    {
        PreferenceManager.getDefaultSharedPreferences(baseContext).edit().putBoolean("notification", enabled).apply();
    }

    public static boolean isNotificationEnabled(Context baseContext)
    {
        return PreferenceManager.getDefaultSharedPreferences(baseContext).getBoolean("notification", true);
    }

    public static void setHours(Context baseContext, int hour)
    {
        PreferenceManager.getDefaultSharedPreferences(baseContext).edit().putInt("hours", hour).apply();
    }

    public static int getHours(Context baseContext)
    {
        return PreferenceManager.getDefaultSharedPreferences(baseContext).getInt("hours", 8);
    }

    public static void setMinutes(Context baseContext, int minute)
    {
        PreferenceManager.getDefaultSharedPreferences(baseContext).edit().putInt("minutes", minute).apply();
    }

    public static int getMinutes(Context baseContext)
    {
        return PreferenceManager.getDefaultSharedPreferences(baseContext).getInt("minutes", 0);
    }
}
